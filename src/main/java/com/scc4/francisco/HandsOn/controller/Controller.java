package com.scc4.francisco.HandsOn.controller;

import com.scc4.francisco.HandsOn.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

//Controlador responsavel pelas requisicoes da chamada "parte 1" da tarefa HandsOn.
@RestController
public class Controller {

    private Service service;

    @Autowired
    public Controller(Service service){
        this.service = service;
    }

    @GetMapping(path = "/listaReversa", produces = "application/json", consumes = "application/json")
    public List<Integer> getListaReversa(@RequestParam(value = "lista") @RequestBody String str){
        return service.reverterLista(str);
    }

    @GetMapping(path = "/imprimirImpares", produces = "application/json", consumes = "application/json")
    public List<Integer> getImpares(@RequestParam(value = "lista") @RequestBody String str){
        return service.impares(str);
    }
    @GetMapping(path = "/imprimirPares", produces = "application/json", consumes = "application/json")
    public List<Integer> getPares(@RequestParam(value = "lista") @RequestBody String str){
        return service.pares(str);
    }

    @GetMapping(path = "/tamanho", produces = "application/json", consumes = "application/json")
    public Integer getTamanhoPalavra(@RequestParam(value="palavra") @RequestBody String str){
        return service.tamanhoPalavra(str);
    }

    @GetMapping(path = "/maiusculas", produces = "application/json", consumes = "application/json")
    public String getCaixaAlta(@RequestParam(value = "palavra") @RequestBody String str){
        return service.palavraCaixaAlta(str);
    }

    @GetMapping(path = "/nomeBibliografico", produces = "application/json", consumes = "application/json")
    public String getNomeBibliografico(@RequestParam(value = "nome") @RequestBody String str){
        return service.palavraBibliografica(str);
    }

    @GetMapping(path = "/vogais", produces = "application/json", consumes = "application/json")
    public List<String> getVogais(@RequestParam(value = "palavra") @RequestBody String str){
        return service.vogais(str);
    }

    @GetMapping(path = "/consoantes", produces = "application/json", consumes = "application/json")
    public List<String> getConsoantes(@RequestParam(value = "palavra") @RequestBody String str){
        return  service.consoantes(str);
    }

    @GetMapping(path = "/cedulas", produces = "application/json", consumes = "application/json")
    public String getCedulas(@RequestParam(value = "total")@RequestBody String str){
        return service.cedulas(str);
    }

    @GetMapping(path = "/calculadora", produces = "application/json", consumes = "application/json")
    public String getCalculo(@RequestParam(value = "valores") @RequestBody String str){
        return service.calculo(str);
    }


}
