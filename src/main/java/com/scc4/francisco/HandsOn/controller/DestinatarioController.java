package com.scc4.francisco.HandsOn.controller;

import com.scc4.francisco.HandsOn.model.bean.Destinatario;
import com.scc4.francisco.HandsOn.model.bean.Remetente;
import com.scc4.francisco.HandsOn.service.DestinatarioService;
import com.scc4.francisco.HandsOn.service.RemetenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("destinatario")
public class DestinatarioController {

    private final DestinatarioService destinatarioService;

    @Autowired
    public DestinatarioController(DestinatarioService destinatarioService) {
        this.destinatarioService = destinatarioService;
    }

    @PostMapping(path = "/create")
    public ResponseEntity<Void> insertListaDestinatarioController(@RequestBody List<Destinatario> lista) {
        boolean flag = destinatarioService.insertListaDestinatarioService(lista);
        System.out.printf("veio");
        if (!flag) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

}
