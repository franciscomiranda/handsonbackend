package com.scc4.francisco.HandsOn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.scc4.francisco.HandsOn.model.bean.Remetente;
import com.scc4.francisco.HandsOn.service.RemetenteService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/remetente")
public class RemetenteController {

    private final RemetenteService remetenteService;

    @Autowired
    public RemetenteController(RemetenteService remetenteService) {
        this.remetenteService = remetenteService;
    }

    @PostMapping(path = "/create")
    public ResponseEntity<Void> insertRemetenteController(@RequestBody Remetente remetente) {
        System.out.println("veio");
        boolean flag = remetenteService.insertRemetenteService(remetente);
        if (!flag) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @GetMapping(path = "/lista")
    @ResponseBody
    public List<Remetente> getListaRemetenteController() {
        return remetenteService.getListaRemetenteService();
    }

    @GetMapping(path = "/listaid/{id}")
    @ResponseBody
    public Remetente getRemetenteById(@PathVariable("id") UUID id){
        return remetenteService.getRemetenteById(id).orElse(null);
    }

    @PutMapping(path = "/update/{id}")
    public boolean updateRemetente(@PathVariable("id") UUID id, @RequestBody Remetente remetente){
        return remetenteService.updateRemetente(id, remetente);
    }

    @DeleteMapping(path = "/delete/{id}")
    public boolean deleteRemetente(@PathVariable("id") UUID id){
        return remetenteService.deleteRemetente(id);
    }

}
