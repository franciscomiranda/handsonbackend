package com.scc4.francisco.HandsOn.model.bean;

public class Calculadora {
    private Fracao fracaoA;
    private Fracao fracaoB;

    public Fracao adicao(Fracao fracaoA, Fracao fracaoB) {
        Fracao fracaoRetorno;
        int mdc = 0;
        if (fracaoA.getDenominador() == fracaoB.getDenominador()) { //Verifica a igualdade do denominador
            fracaoRetorno = new Fracao((fracaoA.getNumerador() + fracaoB.getNumerador()), fracaoA.getDenominador());
        } else {
            fracaoRetorno = new Fracao(((fracaoA.getNumerador() * fracaoB.getDenominador())
                    + fracaoB.getNumerador() * fracaoA.getDenominador()),
                    (fracaoA.getDenominador() * fracaoB.getDenominador()));
        }
        mdc = mdc(fracaoRetorno.getNumerador(), fracaoRetorno.getDenominador());
        fracaoRetorno.setNumerador(fracaoRetorno.getNumerador() / mdc);
        fracaoRetorno.setDenominador(fracaoRetorno.getDenominador() / mdc);
        return fracaoRetorno;
    }

    public Fracao subtracao(Fracao fracaoA, Fracao fracaoB) {
        Fracao fracaoRetorno;
        int mdc = 0;
        if (fracaoA.getDenominador() == fracaoB.getDenominador()) {
            fracaoRetorno = new Fracao((fracaoA.getNumerador() - fracaoB.getNumerador()), fracaoA.getDenominador());
        } else {
            fracaoRetorno = new Fracao(((fracaoA.getNumerador() * fracaoB.getDenominador())
                    - fracaoB.getNumerador() * fracaoA.getDenominador()),
                    (fracaoA.getDenominador() * fracaoB.getDenominador()));
        }
        mdc = mdc(fracaoRetorno.getNumerador(), fracaoRetorno.getDenominador());
        fracaoRetorno.setNumerador(fracaoRetorno.getNumerador() / mdc);
        fracaoRetorno.setDenominador(fracaoRetorno.getDenominador() / mdc);
        return fracaoRetorno;
    }

    public Fracao multiplicacao(Fracao fracaoA, Fracao fracaoB) {
        Fracao fracaoRetorno = new Fracao(fracaoA.getNumerador() * fracaoB.getNumerador(),
                fracaoA.getDenominador() * fracaoB.getDenominador());
        int mdc = 0;
        mdc = mdc(fracaoRetorno.getNumerador(), fracaoRetorno.getDenominador());
        fracaoRetorno.setNumerador(fracaoRetorno.getNumerador() / mdc);
        fracaoRetorno.setDenominador(fracaoRetorno.getDenominador() / mdc);
        return fracaoRetorno;
    }

    public Fracao divisao(Fracao fracaoA, Fracao fracaoB) {
        Fracao fracaoRetorno = new Fracao(fracaoA.getNumerador() * fracaoB.getDenominador(),
                fracaoA.getDenominador() * fracaoB.getNumerador());
        int mdc = 0;
        mdc = mdc(fracaoRetorno.getNumerador(), fracaoRetorno.getDenominador());
        fracaoRetorno.setNumerador(fracaoRetorno.getNumerador() / mdc);
        fracaoRetorno.setDenominador(fracaoRetorno.getDenominador() / mdc);
        return fracaoRetorno;
    }

    public static int mdc(int numerador, int denominador) {
        while (denominador != 0) {
            int resto = numerador % denominador;
            numerador = denominador;
            denominador = resto;
        }
        int mdc = numerador;
        return mdc;
    }

}
