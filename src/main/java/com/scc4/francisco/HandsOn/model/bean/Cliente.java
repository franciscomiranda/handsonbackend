package com.scc4.francisco.HandsOn.model.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

public class Cliente {
    private UUID id;
    private String nome;
    private String cpf;
    private String email;
    private String telefone;

    public Cliente(@JsonProperty("nome") String nome,
                   @JsonProperty("cpf") String cpf,
                   @JsonProperty("email") String email,
                   @JsonProperty("telefone") String telefone) {
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
        this.telefone = telefone;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
