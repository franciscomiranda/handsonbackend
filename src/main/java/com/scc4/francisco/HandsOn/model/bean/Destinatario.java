package com.scc4.francisco.HandsOn.model.bean;

public class Destinatario extends Cliente {
    public Destinatario(String nomeCliente, String cpfCliente, String emailCliente, String telefoneCliente) {
        super(nomeCliente, cpfCliente, emailCliente, telefoneCliente);
    }
}
