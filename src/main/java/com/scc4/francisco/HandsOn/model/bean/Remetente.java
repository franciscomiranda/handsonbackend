package com.scc4.francisco.HandsOn.model.bean;

public class Remetente extends Cliente {

    public Remetente(String nome, String cpf, String email, String telefone) {
        super(nome, cpf, email, telefone);
    }
}
