package com.scc4.francisco.HandsOn.model.dao;

import com.scc4.francisco.HandsOn.model.bean.Destinatario;

import java.util.List;
import java.util.UUID;

public interface DestinatarioDAO {

    boolean insertListaCsv(List<Destinatario> lista);

    List<Destinatario> getLista();

    UUID getUUID();
}
