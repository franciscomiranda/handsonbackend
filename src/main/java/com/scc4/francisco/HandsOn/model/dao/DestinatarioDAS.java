package com.scc4.francisco.HandsOn.model.dao;

import com.scc4.francisco.HandsOn.model.bean.Destinatario;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository("db")
public class DestinatarioDAS implements DestinatarioDAO{
    List<Destinatario> database = new ArrayList<>();

    @Override
    public boolean insertListaCsv(List<Destinatario> listaDestinatario) {
        boolean retorno = false;
        try {
            for (Destinatario destinatario : listaDestinatario) {
                destinatario.setId(getUUID());
            }
            database = listaDestinatario;
            retorno = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return retorno;
        }
    }

    @Override
    public List<Destinatario> getLista() {
        return database;
    }

    @Override
    public UUID getUUID() {
        return UUID.randomUUID();
    }
}
