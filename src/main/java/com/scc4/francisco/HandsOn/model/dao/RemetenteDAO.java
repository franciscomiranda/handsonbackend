package com.scc4.francisco.HandsOn.model.dao;

import com.scc4.francisco.HandsOn.model.bean.Remetente;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface RemetenteDAO {

    boolean insertRemetente(Remetente remetente);

    List<Remetente> getRemetentes();

    Optional<Remetente> getRemetenteById(UUID id);

    boolean deleteRemetente(UUID id);

    boolean updateRemetente(UUID id, Remetente remetente);

    UUID getUUID();
}
