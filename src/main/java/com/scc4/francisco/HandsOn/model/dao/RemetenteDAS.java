package com.scc4.francisco.HandsOn.model.dao;

import com.scc4.francisco.HandsOn.model.bean.Remetente;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("database")
public class RemetenteDAS implements RemetenteDAO {

    List<Remetente> database = new ArrayList<>();

    @Override
    public boolean insertRemetente(Remetente remetente) {
        boolean retorno = false;
        try {
            remetente.setId(getUUID());
            database.add(remetente);
            retorno = true;
        } catch (IllegalArgumentException iaE) {
            iaE.printStackTrace();
        } finally {
            return retorno;
        }
    }

    @Override
    public List<Remetente> getRemetentes() {
        return database;
    }

    @Override
    public Optional<Remetente> getRemetenteById(UUID id) {
        return database.stream().filter(remetente -> remetente.getId().equals(id)).findFirst();
    }

    @Override
    public boolean updateRemetente(UUID id, Remetente r) {
        Optional<Remetente> remetente = getRemetenteById(id);
        boolean retorno = false;
        if(remetente.isPresent()){
            database.remove(remetente.get());
            r.setId(id);
            database.add(r);
            retorno = true;
        }
        return retorno;
    }

    @Override
    public boolean deleteRemetente(UUID id) {
        Optional<Remetente> remetente = getRemetenteById(id);
        boolean retorno = false;
        if (remetente.isPresent()) {
            database.remove(remetente.get());
            retorno = true;
        }
        return retorno;
    }

    @Override
    public UUID getUUID() {
        return UUID.randomUUID();
    }
}
