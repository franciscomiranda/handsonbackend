package com.scc4.francisco.HandsOn.service;

import com.scc4.francisco.HandsOn.model.bean.Destinatario;
import com.scc4.francisco.HandsOn.model.dao.DestinatarioDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DestinatarioService {

    private final DestinatarioDAO destinatarioDAO;

    @Autowired
    public DestinatarioService(@Qualifier("db")DestinatarioDAO destinatarioDAO){
        this.destinatarioDAO = destinatarioDAO;
    }

    public boolean insertListaDestinatarioService(List<Destinatario> lista){
        return destinatarioDAO.insertListaCsv(lista);
    }
}
