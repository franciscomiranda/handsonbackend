package com.scc4.francisco.HandsOn.service;

import com.scc4.francisco.HandsOn.model.dao.RemetenteDAO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.scc4.francisco.HandsOn.model.bean.Remetente;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RemetenteService {

    private final RemetenteDAO remetenteDao;

    @Autowired
    public RemetenteService(@Qualifier("database") RemetenteDAO remetenteDao) {
        this.remetenteDao = remetenteDao;
    }

    public boolean insertRemetenteService(Remetente remetente) {
        return remetenteDao.insertRemetente(remetente);
    }

    public List<Remetente> getListaRemetenteService() {
        return remetenteDao.getRemetentes();
    }

    public Optional<Remetente> getRemetenteById(UUID id){
        return remetenteDao.getRemetenteById(id);
    }

    public boolean updateRemetente(UUID id, Remetente remetente) {
        return remetenteDao.updateRemetente(id, remetente);
    }

    public boolean deleteRemetente(UUID id) {
        return remetenteDao.deleteRemetente(id);
    }
}
