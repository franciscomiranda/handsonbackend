package com.scc4.francisco.HandsOn.service;

import com.scc4.francisco.HandsOn.model.bean.Calculadora;
import com.scc4.francisco.HandsOn.model.bean.Fracao;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@org.springframework.stereotype.Service
public class Service {

    private List<Integer> converterLista(String lst) throws Exception {
        List<String> listaString = new ArrayList<>(Arrays.asList(lst.split(",")));
        List<Integer> listaInteger = new ArrayList<>();
        int i = 0;
        for (String str : listaString) {
            listaInteger.add(Integer.parseInt(str));
        }
        return listaInteger;
    }

    public List<Integer> reverterLista(String lst) {
        List<Integer> listaFinal = new ArrayList<>();
        try {
            listaFinal = converterLista(lst);
            Collections.reverse(listaFinal);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return listaFinal;
    }

    public List<Integer> impares(String lst) {
        List<Integer> listaImpares = new ArrayList<>();
        try {
            List<Integer> lista = converterLista(lst);
            for (Integer numero : lista) {
                if (numero % 2 != 0) {
                    listaImpares.add(numero);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return listaImpares;
    }

    public List<Integer> pares(String str) {
        List<Integer> listaPares = new ArrayList<>();
        try {
            List<Integer> lista = converterLista(str);
            for (Integer numero : lista) {
                if (numero % 2 == 0) {
                    listaPares.add(numero);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return listaPares;
    }

    public Integer tamanhoPalavra(String str) {
        return str.length();
    }

    public String palavraCaixaAlta(String str) {
        return str.toUpperCase();
    }

    public boolean isVowel(String str) {
        String[] vogais = {"a", "e", "i", "o", "u"};
        boolean resultado = false;
        int i;
        for (i = 0; i < 5; i++) {
            if (str.equals(vogais[i])) {
                resultado = true;
            }
        }
        return resultado;
    }

    public List<String> vogais(String str) {
        String[] vetorStr = str.split("");
        List<String> vogaisPalavra = new ArrayList<>();
        int i;
        for (i = 0; i < str.length(); i++) {
            if (isVowel(vetorStr[i])) {
                vogaisPalavra.add(vetorStr[i]);
            }
        }
        return vogaisPalavra;
    }

    public List<String> consoantes(String str) {
        String[] vetorStr = str.split("");
        List<String> consoantesPalavra = new ArrayList<>();
        int i;
        for (i = 0; i < str.length(); i++) {
            if (!isVowel(vetorStr[i])) {
                consoantesPalavra.add(vetorStr[i]);
            }
        }
        return consoantesPalavra;
    }

    public String palavraBibliografica(String str) {
        String[] nomes = str.split("%");
        String nomeBibliografado = null;
        nomeBibliografado = nomes[(nomes.length) - 1].toUpperCase() + ",";
        int i;
        for (i = 0; i < nomes.length - 1; i++) {
            nomeBibliografado += " " + nomes[i];
        }
        return nomeBibliografado;
    }

    public String cedulas(String str) {
        Integer valor = 0;
        Integer cedulaCinco = 0;
        Integer cedulaTres = 0;
        String retorno = null;
        int aux = 0;
        try {
            aux = valor.parseInt(str);
            while (aux % 5 != 0) {
                aux -= 3;
                cedulaTres++;
            }
            cedulaCinco = aux / 5;
            retorno = "Saque R$" + valor + ": " + cedulaTres + " nota de R$3 e " + cedulaCinco + " nota de R$5.";
        } catch (NumberFormatException nfe) {
            retorno = nfe.toString();
        } finally {
            return retorno;
        }
    }

    public String calculo(String str) {
        Calculadora calculadora = new Calculadora();
        List<Integer> valores = null;
        Fracao fracaoA = null;
        Fracao fracaoB = null;
        Fracao fracaoC = null;
        String retorno = null;
        try {
            valores = new ArrayList<>(converterLista(str));
            fracaoA = new Fracao(valores.get(0), valores.get(1));
            fracaoB = new Fracao(valores.get(2), valores.get(3));
            switch (valores.get(4)) {
                case 1:
                    fracaoC = calculadora.adicao(fracaoA, fracaoB);
                    break;
                case 2:
                    fracaoC = calculadora.subtracao(fracaoA, fracaoB);
                    break;
                case 3:
                    fracaoC = calculadora.multiplicacao(fracaoA, fracaoB);
                    break;
                case 4:
                    fracaoC = calculadora.divisao(fracaoA, fracaoB);
                    break;
            }
            retorno = fracaoC.toString();
        } catch (Exception ex) {
            retorno = ex.toString();
        }
        return retorno;
    }
}
