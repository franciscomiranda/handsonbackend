CREATE SCHEMA handsondatabase;
USE handsondatabase;
CREATE TABLE remetente(
	idremetente INT NOT NULL UNIQUE AUTO_INCREMENT,
    nome_remetente VARCHAR(50) NOT NULL,
    cpf_remetente VARCHAR(11) NOT NULL,
    email_remetente VARCHAR(60) NOT NULL,
    telefone_remetente VARCHAR(11) NOT NULL,
    PRIMARY KEY(idremetente));
    
DELIMITER $$
CREATE PROCEDURE sp_insert_remetente(
	IN nomeRemetente VARCHAR(50),
    IN cpfRemetente VARCHAR(11),
    IN emailRemetente VARCHAR(60),
    IN telefoneRemetente VARCHAR(11))
	BEGIN
		INSERT INTO remetente(
		nome_remetente,
        cpf_rementente,
        email_remetente,
        telefone_remetente)
			VALUES(
            nomeRemetente,
            cpfRemetente,
            emailRemetente,
            telefoneRemetente);
	END $$
DELIMITER $$